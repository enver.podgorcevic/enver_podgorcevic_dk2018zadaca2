
#include <iostream>
#include <vector>

#include "token.hpp"
#include "parser.h"
#include "lex.yy.c"

using namespace std;
int main(void)
{
    vector<Token> tokens;
    vector<int> lineNums;

    int yytoken;

    while( yytoken = yylex() ){
        tokens.push_back(Token(yytoken, yytext));
        lineNums.push_back(lineCount);
    }

    Parser p(tokens);

    if(p.parse())
        cout << "Input fajl validan" << endl;
    else 
        cout << "Input fajl nije validan, greška u liniji " << lineNums[p.currentIndex] << endl;
    return 0;
}

