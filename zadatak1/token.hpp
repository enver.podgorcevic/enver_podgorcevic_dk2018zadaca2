
#ifndef _TOKEN_
#define _TOKEN_

#include <string>
#include <iostream>

const int TAGBEGIN = 1;
const int TAGEND = 2;
const int TAGCLOSE = 3;
const int TAGENDANDCLOSE = 4;
const int ATTRIBUTENAME = 5;
const int EQUAL = 6;
const int ATTRIBUTEVALUE = 7;
const int CONTENT = 8;
const int QUOTES = 9;
const int WS = 10;

class Token{
    public:
        int tag;
        std::string lexeme;
        Token(int i=0, std::string const & s=""):tag(i), lexeme(s) {}
        Token& operator=(Token const & t){
            tag = t.tag;
            lexeme = t.lexeme;
            return *this;
        }
        void print(){
            std::cout << "<" << tag << ", " << lexeme << ">" << std::endl;
        }
        std::string info(){
            std::string returnString = "<";
            switch(tag){
                case TAGBEGIN:
                    returnString += "TAGBEGIN";
                    break;
                case TAGEND:
                    returnString += "TAGEND";
                    break;
                case TAGCLOSE:
                    returnString += "TAGCLOSE";
                    break;
                case TAGENDANDCLOSE:
                    returnString += "TAGENDANDCLOSE";
                    break;
                case ATTRIBUTENAME:
                    returnString += "ATTRIBUTENAME";
                    break;
                case EQUAL:
                    returnString += "EQUAL";
                    break;
                case ATTRIBUTEVALUE:
                    returnString += "ATTRIBUTEVALUE";
                    break;
                case CONTENT:
                    returnString += "CONTENT";
                    break;
            }
            returnString += ", " + lexeme + ">:";
            return returnString;
        }
};
#endif
