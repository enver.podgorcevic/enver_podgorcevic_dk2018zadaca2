#pragma once

#include <string>

const int PROGRAM = 1;
const int TYPE = 2;
const int END = 3;
const int ENDL = 4;
const int INT = 5;
const int FLOAT = 6;
const int STRING = 7;

class Token
{
  public:
  int tag;
  std::string lexeme;
  int line_num;

  Token(int i = 0, std::string const & s = "", int ln = 0)
    : tag{i}, lexeme{s}, line_num{ln}
  {}
};
