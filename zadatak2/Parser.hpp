#pragma once

#include <iostream>
#include <vector>

#include "Token.hpp"

class Parser
{
  public:
  std::vector<Token> tokens;
  size_t currentIndex = 0;
  size_t bufferLength = 0;
  int line_num = 0;

  Parser(std::vector<Token> const & t)
    : tokens{t}, bufferLength{t.size()}
  {}

  bool parse();
  bool epsilon();
  bool terminal(int, std::string* = nullptr);
  bool program();
  bool header();
  bool template_();
  bool body_section();
  bool footer();
  bool typelines();
  bool typelines1();
  bool typeline();
  bool body_lines();
  bool body_lines1();
  bool body_line();
  bool ENDLS();
  bool ENDLS1();
};