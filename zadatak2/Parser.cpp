
#include "Parser.hpp"

using namespace std;

bool Parser::parse()
{
    currentIndex = 0;
    bool ret = program();
    if (currentIndex < bufferLength)
        ret = false;
    if (!ret)
    {
        cout << "Input fajl nije validan, greška u liniji " << line_num << endl;
        exit(-1);
    }
    return ret;
}

bool Parser::epsilon()
{
    return true;
}

bool Parser::terminal(int t, std::string* lval)
{
    line_num = tokens[currentIndex].line_num;
    if (currentIndex >= bufferLength)
        return false;
    if (t == tokens[currentIndex].tag)
    {
        if (lval)
            *lval = tokens[currentIndex].lexeme;
        ++currentIndex;
        return true;
    }
    else
        return false;
}

bool Parser::program()
{
    bool ret = header() && template_() && body_section() && footer(); 
    if (ret)
        cout << "done with a program file!" << endl;
    return ret;
}

bool Parser::header()
{
    string lval;
    bool ret = terminal(PROGRAM) && terminal(FLOAT, &lval) && ENDLS();
    if (ret)
        cout << "reading a program file version " << lval << endl;
    return ret;
}

bool Parser::template_()
{
    return typelines();
}

bool Parser::typelines()
{
    return typeline() && typelines1();
}

bool Parser::typelines1()
{
    size_t save = currentIndex;
    return (typeline() && typelines1())
        || (currentIndex = save, epsilon());
}

bool Parser::typeline()
{
    string lval;
    bool ret = terminal(TYPE) && terminal(STRING, &lval) && ENDLS();

    if (ret)
        cout << "new defined program type: " << lval << endl;
    return ret;
}

bool Parser::body_section()
{
    return body_lines();
}

bool Parser::body_lines()
{
    return body_line() && body_lines1();
}

bool Parser::body_lines1()
{
    size_t save = currentIndex;
    return (body_line() && body_lines1())
        || (currentIndex = save, epsilon());
}

bool Parser::body_line()
{
    string lval1, lval2, lval3, lval4, lval5;
    bool ret = terminal(INT, &lval1) && terminal(INT, &lval2) 
        && terminal(INT, &lval3) && terminal(INT, &lval4) 
        && terminal(STRING, &lval5) && ENDLS();
    if (ret)
        cout << "new program line: " << lval1 << lval2 << lval3 << lval4 << lval5
            << endl;
    return ret;
}

bool Parser::footer()
{
    return terminal(END) && ENDLS();
}

bool Parser::ENDLS()
{
    return terminal(ENDL) && ENDLS1();
}

bool Parser::ENDLS1()
{
    size_t save = currentIndex;
    return (terminal(ENDL) && ENDLS1())
        || (currentIndex = save, epsilon());
}
