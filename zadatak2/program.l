%{
#define YY_DECL extern "C" int yylex()
#include "Token.hpp"
int line_num = 1;
%}
%%
[ \t]          {}
Program        {return PROGRAM;}
type           {return TYPE;}
end            {return END;}
[0-9]+\.[0-9]+ {return FLOAT;}
[0-9]+         {return INT;}
[a-zA-Z0-9]+   {return STRING;}
\n             {++line_num; return ENDL;}
.              {}
%%
