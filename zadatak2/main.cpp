#include "Parser.hpp"

extern int line_num;
extern "C" int yylex();
extern "C" char *yytext;

int main(int argc, char const *argv[])
{
  int tag;
  std::vector<Token> tokens; 
  while (tag = yylex())
    tokens.emplace_back(tag, yytext, line_num);
  
  Parser parser{tokens};
  parser.parse();

  std::cout << "Input fajl validan" << std::endl;
  return 0;
}
