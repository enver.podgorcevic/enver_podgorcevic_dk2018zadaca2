
%{

#include <cstdio>
#include <iostream>
using namespace std;

extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;
extern int line_num;

void yyerror(const char *s);

%}

%token TAGBEGIN
%token TAGEND
%token TAGCLOSE
%token TAGENDANDCLOSE
%token ATTRIBUTENAME
%token EQUAL
%token ATTRIBUTEVALUE
%token CONTENT

%%

xml:
  tag xml
  | %empty
  ;

tag:
  open content close
  | openclose
  ;

open:
  TAGBEGIN attribute TAGEND
  ;

attribute:
  ATTRIBUTENAME EQUAL ATTRIBUTEVALUE attribute
  | %empty
  ;

content:
  text
  | xml
  ;

text:
  CONTENT text
  | %empty
  ;

close:
  TAGCLOSE
  ;

openclose:
  TAGBEGIN attribute TAGENDANDCLOSE
  ;

%%

int main()
{
  do {
    yyparse();
  } while (!feof(yyin));

cout << "Input fajl validan" << endl;

return 0;
}

void yyerror(const char *s)
{
  cout << "Input fajl nije validan, greška u liniji " << line_num << endl;
  exit(-1);
}
